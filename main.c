#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <time.h>
#include "inicializacao.h"
#include "skins.h"

/* struct para contar tempo em microsegundos */
struct timespec tms;

int main(){
	int nlin, ncol, id_atual,id_atual_tiros, lin, col, velocidade_atual, id_jogador;
	int key, nave_mae_timing, placar;
    int contador, id_navemae, velocidade_aliens, velocidade_MAXIMA_aliens;
    t_lista lista_objetos, lista_tiros, lista_tiros_aliens;
    /* define um tempo aleatorio para a passagem da nave mae */
    nave_mae_timing = 50000 + rand() % 200000;

    placar = 0;

    velocidade_aliens = 4000;
    velocidade_MAXIMA_aliens = 434;

	srand(time(NULL));

    initscr();              /* inicia a tela */
    keypad(stdscr, TRUE);   /* permite a leitura das setas */
    cbreak();               /* desabilita o buffer de entrada */
    noecho();              /* não mostra os caracteres digitados */
    nodelay(stdscr, TRUE);  /* faz com que getch não aguarde a digitação */
    curs_set(FALSE);        /* não mostra o cursor na tela */
	start_color();

	/* criando os pares de cores */
	init_color(COLOR_BLACK, 0, 0, 0);
	init_pair(1, COLOR_CYAN, COLOR_BLACK);
  	init_pair(2, 93, COLOR_BLACK);
  	init_pair(3, COLOR_RED, COLOR_BLACK);
  	init_pair(4, 40, COLOR_BLACK);
  	init_pair(5, 9, COLOR_BLACK);
  	init_pair(6, 94, COLOR_BLACK);
  	init_pair(8, 11, COLOR_BLACK);
  	init_pair(9, COLOR_BLUE, COLOR_BLACK);
	/* obtendo o tamanho do terminal e armazenando o nlin ncol de acordo com o tam  */
    getmaxyx(stdscr, nlin, ncol);
    if((nlin < 38) || (ncol < 100)){
    	endwin();
    	printf("Seu terminal deve ter, no minimo, 38 linhas e 100 colunas!\n");
    	return 0;
    }

    /* inicializando o ambiente grafico do jogo */
    inicializa_lista(&lista_objetos);
    inicializa_lista(&lista_tiros);
    inicializa_lista(&lista_tiros_aliens);
    id_atual = 2;
    id_navemae = 1;
    id_atual_tiros = 1;
    inicializa_navemae(&id_atual, &lista_objetos);
    inicializa_binding(&id_atual, &lista_objetos);
    inicializa_aliens(&id_atual, &lista_objetos, velocidade_aliens);
    inicializa_jogador(&id_atual, &lista_objetos, &id_jogador);
    contador = 1;
 while(1){
 	wattron(stdscr, COLOR_PAIR(8));
    move(1,44);
    printw("SCORE:");
    move(1,50); 
    printw("%06d", placar);
    wattroff(stdscr, COLOR_PAIR(5));

    if(contador >= 500000){
    	contador = 1;
    }

    /* registrando o tempo em microsegundos */
    if (clock_gettime(CLOCK_REALTIME,&tms)) {
        return -1;
    }
    /* segundos, multiplicado por 10^-6 */
    int64_t micros = tms.tv_sec * 1000000;
    micros += tms.tv_nsec/1000;
    /* arredondando, caso seja necessario */
    if (tms.tv_nsec % 1000 >= 500) {
        micros++;
    }

    if(game_over(&lista_objetos,&lista_tiros, &lista_tiros_aliens, &id_atual, &velocidade_aliens)){
    	break;
    }
    /* imprime a tela de jogo */
    imprime_nave_mae_lista(&lista_objetos);
    desenha_borda();
    deleta_explosao(&lista_objetos, micros);
    imprime_lista(&lista_objetos);
    imprime_lista(&lista_tiros);
    imprime_lista(&lista_tiros_aliens);
	spawna_tiros_dos_aliens(&lista_tiros_aliens, &lista_objetos, &id_atual, &id_atual_tiros);
    consulta_gameover_tipo(ALIEN3_SKIN2, &lista_objetos);

    /* verifica qual a tecla pressionada */
    key = getch();

    if(contador % nave_mae_timing == 0){
    	spawna_navemae(&lista_objetos, id_navemae);
    	nave_mae_timing = 50000 + rand() % 200000;
    		srand(time(NULL));
    }
    if(contador % SPEEDNAVEMAE == 0){
    	movimenta_navemae(&lista_objetos);
    }
    if(contador % SPEEDTIRO_ALIENS == 0){
    /* atualiza a lista de tiros dos aliens, movendo eles para baixo */
        atualiza_lista(BAIXO, &lista_tiros_aliens);
    }
    if(contador % SPEEDTIRO == 0){
        atualiza_lista(CIMA, &lista_tiros);
        detecta_colisao_tiro(&lista_tiros, &lista_objetos, micros, &id_atual, &placar);
        detecta_colisao_tiro(&lista_tiros_aliens, &lista_objetos, micros, &id_atual, &placar);
    }

    consulta_velocidade_tipo(ALIEN1, &velocidade_atual, &lista_objetos);

    /* limitando a velocidade dos aliens apos um certo momento */
    if(velocidade_atual <= velocidade_MAXIMA_aliens){
    	velocidade_atual = velocidade_MAXIMA_aliens;
    }
    if(contador % velocidade_atual == 0){
    	troca_skin(&lista_objetos);
   		movimenta_aliens(&lista_objetos);
    	detecta_colisao_alien(&lista_objetos, &id_atual, micros);
    }

    /* detecta a tecla */
    if((key == 'q') || (key == 'Q')){
 	  endwin();
      destroi_lista(&lista_objetos);
      destroi_lista(&lista_tiros);
 	  exit(1);
    }
    else if((key == KEY_RIGHT) || (key == 'd')){
            atualiza_item_lista(id_jogador, DIREITA, &lista_objetos);
            erase();
    }
    else if((key == KEY_LEFT) || (key == 'a')){
            atualiza_item_lista(id_jogador, ESQUERDA, &lista_objetos);
            erase();
    }
    /* atira */
    else if((key == KEY_UP) || (key == ' ')){
            consulta_posicao_id(id_jogador, &col, &lin, &lista_objetos);
            insere_fim_lista(TIRO, id_atual_tiros, &lista_tiros, lin-1, col+2, micros, 0,0);
            id_atual_tiros++;
            erase();

    }

    /* incrementa o contador de mod e limpa a tela */
    erase();   	
    contador++;
 
 	/* congela o while por 1 micro segundo para nao ter gasto excessivo de CPU */
    usleep(1);

 }

	/* destroi todas as listas pelo game over */
 	destroi_lista(&lista_tiros_aliens);
    destroi_lista(&lista_objetos);
    destroi_lista(&lista_tiros);

    erase();
    refresh();
 	wattron(stdscr, COLOR_PAIR(8));
    move(15,50);
    printw("GAME OVER!");
    move(16,47);
    printw("Seu score: %06d", placar);
    move(18,34);
    printw("Obrigado por jogar, pressione 'q' para sair. ");
    refresh();
 	wattroff(stdscr, COLOR_PAIR(8));  
 	
 	while(1){
    	key = getch();
 		if((key == 'q') || (key == 'Q')){
 			delwin(stdscr);
   			endwin();
 			exit(1);
 		}

 		usleep(1);
 	
 	}

    return 0;
}
