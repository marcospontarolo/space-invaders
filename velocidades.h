/* 
neste arquivo sao definidas as velocidades dos objetos.
A velocidade dos objetos toma como base o contador que está
na main MOD a velocidade definida aqui, simulando um timestamp.
*/

/* velocidade dos aliens */
#define SPEEDALIEN 3000

/* velocidade da nave mae */
#define SPEEDNAVEMAE 400

/* velocidade do canhao, jogador */
#define SPEEDJOGADOR 1000

/* velocidade da movimentacao do tiro na tela */
#define SPEEDTIRO 450

/* velocidade da movimentacao do tiro na tela */
#define SPEEDTIRO_ALIENS 1000

/* delay entre tiros EM MICROSEGUNDOS */
#define DELAYTIRO 150000

/* delay da explosao na tela EM MICROSEGUNDOS */
#define DELAY_EXPLOSAO  90000
	