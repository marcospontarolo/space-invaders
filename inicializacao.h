
/* configura o cabecalho de todas as funcoes de inicializacao */

#include "movimentacao.h"

void inicializa_aliens(int *id_atual, t_lista *l, int velocidade_aliens);

void inicializa_jogador(int *id_atual, t_lista *l, int *id_jogador);

void inicializa_navemae(int *id_atual, t_lista *l);

/* inicializa lugares que nao poderao ser acertados com tiros */
void inicializa_binding(int *id_atual, t_lista *l);

void desenha_borda();

/* verifica se houve um game over, se o jogador matou todos os aliens aumnenta a velocidade deles */
int game_over(t_lista *l, t_lista *a, t_lista *b, int *id_atual, int *velocidade_aliens);

void spawna_navemae(t_lista *l, int id_navemae);