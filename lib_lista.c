/* implementa as funcoes da lib_lista.h para o jogo space invaders
*/
#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include "lib_lista.h"
#include "skins.h"

int inicializa_lista(t_lista *l){
	t_nodo *sentinela_inicio=NULL, *sentinela_final=NULL;

	/* alocando espaço para as sentinelas */
	sentinela_final = (t_nodo *) malloc(sizeof(t_nodo));
	sentinela_inicio = (t_nodo *) malloc(sizeof(t_nodo));
	/* caso NULL, falta memória */
	if((sentinela_final == NULL) || (sentinela_inicio == NULL)){
		printf("ERRO: Memoria insuficiente.");
		return 0;
	}

	/*inicializa ini,fim,tam e atual da lista para as sentinelas */
	l->ini = sentinela_inicio;
	l->fim = sentinela_final;
	l->atual = NULL;
	l->tamanho = 0;

	/* aponta as sentinelas para elas mesmas, após inicado*/
	sentinela_inicio->prox = sentinela_final;
	sentinela_final->prev = sentinela_inicio;
	sentinela_inicio->sentido = RIGHT;
	sentinela_inicio->elemcol = 1;

	return 1;
}

int lista_vazia(t_lista *l){
	if(l->tamanho == 0)
		return 1;
	else
		return 0;
}
int insere_depois_da_barreira_lista(int item, int id, t_lista *l, int lin, int col, int timestamp, int velocidade){
	t_nodo *novo=NULL;

	novo = (t_nodo *) malloc(sizeof(t_nodo));

	if(novo == NULL){
		printf("ERRO: Memoria insuficiente.");
		return 0;
	}
	int i;

	l->atual = l->ini->prox;
	for(i = 1; i <= 3; i++){
		l->atual = l->atual->prox;
	}

	/* configurando o novo nodo */
	novo->prox = l->atual->prox;
	novo->prev = l->atual;
	novo->tipoelem = item;
	novo->elemlin = lin;
    novo->elemcol = col;
    novo->timestamp = timestamp;
    novo->estado = 1;
    novo->id = id;

	l->atual->prox->prev= novo;
	l->atual->prox = novo;

	l->tamanho++;

	return 1;

}
int insere_fim_lista(int item, int id, t_lista *l, int lin, int col, int timestamp, int velocidade, int coluna){
	t_nodo *novo=NULL;
	int conta;

	if(item == TIRO){
		if(l->fim->prev != l->ini){
			
			/* cria um delay entre os tiros em jogo */
			conta = timestamp - l->fim->prev->timestamp;
			if (conta < DELAYTIRO){
				return 0;
			}

			/* limita o numero de tiros em jogo */
			int tam_lista;
			tamanho_lista(&tam_lista, l);
			if(tam_lista == 3)
				return 0;

		}
	}
	novo = (t_nodo *) malloc(sizeof(t_nodo));

	if(novo == NULL){
		printf("ERRO: Memoria insuficiente.");
		return 0;
	}

	/* configurando o novo nodo */
	novo->prox = l->fim;
	novo->prev = l->fim->prev;
	novo->speed = velocidade;
	novo->tipoelem = item;
	novo->elemlin = lin;
    novo->elemcol = col;
    novo->timestamp = timestamp;
    novo->estado = 1;
    novo->id = id;
    novo->coluna = coluna;

	l->fim->prev->prox = novo;
	l->fim->prev = novo;

	l->tamanho++;

	return 1;
}





int remove_item_lista(int id, t_lista *l){
	if(lista_vazia(l)){
		return 0;
	}

	l->atual = l->ini->prox;
	l->fim->id = id;

	while(l->atual->id != id){
		incrementa_atual(l);
	}

	if(l->atual == l->fim){
		return 0;
	}
	else{
		t_nodo *remover;

		remover = l->atual;

		l->atual->prev->prox = l->atual->prox;
		l->atual->prox->prev = l->atual->prev;
		l->atual = NULL;

		l->tamanho--;

		free(remover);

		return 1;		
	}

	return 0;
}

int atualiza_lista( int direcao, t_lista *l){
	if(lista_vazia(l)){
		return 0;
	}
		int i, tam_lista, idremover;

		tamanho_lista(&tam_lista,l);
	/* move o objeto na direcao desejada */
	l->atual = l->ini->prox;
	for(i = 1; i <= tam_lista; i++){

		if((l->atual->elemlin == 2) || (l->atual->elemlin == 36)){
			idremover = l->atual->id;
			t_nodo *reserva;
			reserva = l->atual->prox;
			remove_item_lista(idremover, l);
			l->atual = reserva;
		}
		else if(direcao == DIREITA){
			l->atual->elemcol++;
			l->atual = l->atual->prox;
		}
		else if(direcao == ESQUERDA){
			l->atual->elemcol--;
			l->atual = l->atual->prox;
		}
		else if(direcao == CIMA){
			l->atual->elemlin--;
			l->atual = l->atual->prox;
		}
		else if(direcao == BAIXO){
			l->atual->elemlin++;
			l->atual = l->atual->prox;
		}

	}
		return 1;
}


int atualiza_item_lista(int id, int direcao, t_lista *l){
	if(lista_vazia(l)){
		return 0;
	}

	l->atual = l->ini->prox;
	l->fim->id = id;

	while(l->atual->id != id){
		incrementa_atual(l);
	}

	if(l->atual == l->fim){
		return 0;
	}else{

		if((l->atual->elemcol == 1) && (direcao == ESQUERDA)){
			return 0;
		}
		else if((l->atual->elemcol == 95) && (direcao == DIREITA)){
			return 0;
		}

		/* move o objeto na direcao desejada */
		if(direcao == DIREITA){
			l->atual->elemcol++;
		}
		else if(direcao == ESQUERDA){
			l->atual->elemcol--;
		}
		else if(direcao == CIMA){
			l->atual->elemlin--;
		}

		return 1;		
	}

	return 0;
}

void incrementa_atual(t_lista *l){
	if(lista_vazia(l))
		l->atual = NULL;
	else if(l->atual == NULL)
		l->atual = l->ini->prox;
	else if(l->atual == l->fim)
		l->atual = NULL;
	else
		l->atual = l->atual->prox;

}

void consulta_posicao_id(int id, int *col, int *lin, t_lista *l){
	if(!lista_vazia(l)){

		l->atual = l->ini->prox;
		l->fim->id = id;

		while(l->atual->id != id){
			incrementa_atual(l);
		}

		if(!(l->atual == l->fim)){
			*col = l->atual->elemcol;
			*lin = l->atual->elemlin;
		}
		else{
			*col = -1;
			*lin = -1;
		}
	}
}
void consulta_alien_id(int id, int *col, int *lin, t_lista *l){
	if(!lista_vazia(l)){

		l->atual = l->ini->prox;
		l->fim->id = id;
		l->fim->tipoelem = ALIEN3_SKIN2;

		while((l->atual->id != id) || (l->atual->tipoelem > ALIEN3_SKIN2)){
			incrementa_atual(l);
		}

		if(l->atual != l->fim){
			*col = l->atual->elemcol;
			*lin = l->atual->elemlin;
		}
		else{
			*col = -1;
			*lin = -1;
		}
	}
}
void consulta_velocidade_tipo(int tipo, int *velocidade, t_lista *l){
	if(!lista_vazia(l)){
		l->atual = l->ini->prox;
		l->fim->tipoelem = tipo;

		while(l->atual->tipoelem != tipo){
			incrementa_atual(l);
		}

		if(!(l->atual == l->fim)){
			*velocidade = l->atual->speed;
		}	
	}
}
void consulta_gameover_tipo(int tipo, t_lista *l){
	if(!lista_vazia(l)){

		l->atual = l->ini->prox;
		l->fim->tipoelem = tipo;

		while(l->atual->tipoelem > tipo){
			incrementa_atual(l);
		}

		if(l->atual == l->fim){
		l->ini->tipoelem = VENCEU;
		}
	}
}

void reseta_barreira(t_lista *l){
	if(!lista_vazia(l)){
		l->atual = l->ini->prox;

		while(l->atual != l->fim){
			if(l->atual->tipoelem == NOTHING){
				t_nodo *remover;
				t_nodo *prox;
				prox = l->atual->prox;

				remover = l->atual;

				l->atual->prev->prox = l->atual->prox;
				l->atual->prox->prev = l->atual->prev;
				l->tamanho--;
				free(remover);
				l->atual = prox;
		    }
		    else{
		 		incrementa_atual(l);
		    }
		}	
	}
}

void imprime_lista(t_lista *l){
	if(lista_vazia(l)){
		/* nao faz nada */
	}
	else{

		int id, lin, col, tipoelem, estado;
		int i;
		int tam_lista;

		l->atual = l->ini->prox;
		tamanho_lista(&tam_lista,l);

		for(i = 1; i <= tam_lista; i++){

			consulta_item_atual(&id, &lin, &col, &tipoelem, &estado, l);
			if(tipoelem == NOTHING){
				move(lin, col);
				addstr(" ");
			}
			else if(tipoelem == NOTHING_PERMANENTE){
				move(lin, col);
				addstr(" ");
			}
			else if(tipoelem == BARREIRA){
				wattron(stdscr, COLOR_PAIR(4));
				move(lin, col);
				addstr(BARREIRA_CABECA);
				move(lin+1, col);
				addstr(BARREIRA_TRONCO);
				move(lin+2, col);
				addstr(BARREIRA_PE);
				wattroff(stdscr, COLOR_PAIR(4));
			}
			else if(tipoelem == ALIEN1){
				wattron(stdscr, COLOR_PAIR(2));
				move(lin, col);
				addstr(ALIEN1_CABECA);
				move(lin+1, col);
				addstr(ALIEN1_TRONCO);
				move(lin+2, col);
				addstr(ALIEN1_PE);
				wattroff(stdscr, COLOR_PAIR(2));
			}
			else if(tipoelem == ALIEN1_SKIN2){
				wattron(stdscr, COLOR_PAIR(2));
				move(lin, col);
				addstr(ALIEN1_CABECA);
				move(lin+1, col);
				addstr(ALIEN1_TRONCO);
				move(lin+2, col);
				addstr(ALIEN1_SKIN2_PE);
				wattroff(stdscr, COLOR_PAIR(2));
			}
			else if(tipoelem == ALIEN2){
				wattron(stdscr, COLOR_PAIR(9));
				move(lin, col);
				addstr(ALIEN2_CABECA);
				move(lin+1, col);
				addstr(ALIEN2_TRONCO);
				move(lin+2, col);
				addstr(ALIEN2_PE);
				wattroff(stdscr, COLOR_PAIR(9));
			}
			else if(tipoelem == ALIEN2_SKIN2){
				wattron(stdscr, COLOR_PAIR(9));
				move(lin, col);
				addstr(ALIEN2_CABECA);
				move(lin+1, col);
				addstr(ALIEN2_TRONCO);
				move(lin+2, col);
				addstr(ALIEN2_SKIN2_PE);
				wattroff(stdscr, COLOR_PAIR(9));
			}
			else if(tipoelem == ALIEN3){
				wattron(stdscr, COLOR_PAIR(1));
				move(lin, col);
				addstr(ALIEN3_CABECA);
				move(lin+1, col);
				addstr(ALIEN3_TRONCO);
				move(lin+2, col);
				addstr(ALIEN3_PE);
				wattroff(stdscr, COLOR_PAIR(1));
			}
			else if(tipoelem == ALIEN3_SKIN2){
				wattron(stdscr, COLOR_PAIR(1));
				move(lin, col);
				addstr(ALIEN3_CABECA);
				move(lin+1, col);
				addstr(ALIEN3_TRONCO);
				move(lin+2, col);
				addstr(ALIEN3_SKIN2_PE);
				wattroff(stdscr, COLOR_PAIR(1));
			}
			else if(tipoelem == JOGADOR){
				move(lin, col);
				addstr(JOGADOR_CABECA);
				move(lin+1, col);
				addstr(JOGADOR_PE);
			}
			else if(tipoelem == TIRO){
				wattron(stdscr, COLOR_PAIR(6));
				move(lin, col);
				addstr("|");
				wattroff(stdscr, COLOR_PAIR(6));
			}
			else if(tipoelem == TIRO_ALIEN){
				wattron(stdscr, COLOR_PAIR(8));
				move(lin, col);
				addstr("$");
				wattroff(stdscr, COLOR_PAIR(8));
			}
			else if(tipoelem == EXPLOSAO){
				wattron(stdscr, COLOR_PAIR(5));
				move(lin, col);
				addstr(EXPLOSAO_CABECA);
				move(lin+1, col);
				addstr(EXPLOSAO_TRONCO);
				move(lin+2, col);
				addstr(EXPLOSAO_PE);
				wattroff(stdscr, COLOR_PAIR(5));
			}
			incrementa_atual(l);
			tamanho_lista(&tam_lista,l);

		}

	}
}

void imprime_nave_mae_lista(t_lista *l){
	if(lista_vazia(l)){
		/* nao faz nada */
	}
	else{

		int id, lin, col, tipoelem, estado;
		int i;
		int tam_lista;

		l->atual = l->ini->prox;
		tamanho_lista(&tam_lista,l);

		for(i = 1; i <= tam_lista; i++){
			consulta_item_atual(&id, &lin, &col, &tipoelem, &estado, l);
			if(tipoelem == NAVEMAE){
			wattron(stdscr, COLOR_PAIR(3));

			if((col >= 1) && (col <= 99)){
				move(lin, col);
				addstr(NAVEMAE_SKIN1);
				move(lin+1, col);
				addstr(NAVEMAE_SKIN2);
				move(lin+2, col);
				addstr(NAVEMAE_SKIN3);
			}
			if((col >= 2) &&  (col <= 101)){
				move(lin, col-2);
				addstr(NAVEMAE_SKIN4);
				move(lin+1, col-2);
				addstr(NAVEMAE_SKIN5);
				move(lin+2, col-2);
				addstr(NAVEMAE_SKIN6);	
			}
			if((col >= 4) &&  (col <= 103)){
				move(lin, col-4);
				addstr(NAVEMAE_SKIN7);
				move(lin+1, col-4);
				addstr(NAVEMAE_SKIN8);
				move(lin+2, col-4);
				addstr(NAVEMAE_SKIN9);	
			}
			if((col >= 6) &&  (col <= 105)){
				move(lin, col-6);
				addstr(NAVEMAE_SKIN10);
				move(lin+1, col-6);
				addstr(NAVEMAE_SKIN11);
				move(lin+2, col-6);
				addstr(NAVEMAE_SKIN12);	
			}
			if((col >= 8) &&  (col <= 107)){
				move(lin, col-7);
				addstr(NAVEMAE_SKIN13 );
				move(lin+1, col-7);
				addstr(NAVEMAE_SKIN14);
				move(lin+2, col-7);
				addstr(NAVEMAE_SKIN15);	
			}

			}
			wattroff(stdscr, COLOR_PAIR(3));
			incrementa_atual(l);
			tamanho_lista(&tam_lista,l);

		}

	}
}

int tamanho_lista(int *tam, t_lista *l){
	*tam = l->tamanho;
	return 1;
}



int consulta_item_atual(int *id,int *lin, int *col, int *tipoelem, int *estado, t_lista *atual){
	if(lista_vazia(atual)){
		printf("Erro: Lista vazia! \n");
		return 0;
	}

	if(atual->atual == NULL)
		return 0;
	else if(atual->atual == atual->ini)
		return 0;
	else if(atual->atual  == atual->fim)
		return 0;
	else{
		*id = atual->atual->id;
		*lin = atual->atual->elemlin;
		*col = atual->atual->elemcol;
		*tipoelem = atual->atual->tipoelem;
		*estado = atual->atual->estado;
	}
	return 1;
}


void destroi_lista(t_lista *l){
	t_nodo *sentinela_inicio=NULL, *sentinela_final=NULL;


	/* checa se a lista esta vazia, se sim, retorna erro. */
	if(!lista_vazia(l)){
		while(!lista_vazia(l))
			remove_fim_lista(l);
		
		/* agora iremos liberar as sentinelas alocadas */
		sentinela_final = l->fim;
		sentinela_inicio = l->ini;
		free(sentinela_inicio);
		free(sentinela_final);
	
	}

}

int remove_fim_lista(t_lista *l){
	if(lista_vazia(l)){
		return 0;
	}

	t_nodo *remover=NULL;

	remover = l->fim->prev;

	l->fim->prev->prev->prox = l->fim;
	l->fim->prev = l->fim->prev->prev;

	l->tamanho--;

	free(remover);

	return 1;

}



int deleta_explosao(t_lista *l, int timestamp_atual){
	if(lista_vazia(l)){
		return 0;
	}

	l->atual = l->ini->prox;
	l->fim->tipoelem = EXPLOSAO;

	while(l->atual->tipoelem != EXPLOSAO){
		incrementa_atual(l);
	}
	if((l->atual == l->fim) || ( (timestamp_atual - l->atual->timestamp) <= DELAY_EXPLOSAO  )){
		return 0;
	}
	else{
		t_nodo *remover;

		remover = l->atual;

		l->atual->prev->prox = l->atual->prox;
		l->atual->prox->prev = l->atual->prev;
		l->atual = NULL;

		l->tamanho--;

		free(remover);

		return 1;		
	}

	return 0;	
}

void consulta_id_ultimo(t_lista *l, int *id){
	if(lista_vazia(l)){
		*id = 1;
	}
	else{
		*id = l->fim->prev->id;
	}

}



void spawna_tiros_dos_aliens(t_lista *l_tiros, t_lista *l_aliens, int *id_atual, int *id_atual_tiros){
	int tam_lista, lin, col;

	tamanho_lista(&tam_lista, l_tiros);
	if(tam_lista < 3){
	
	/* gera um numero aleatorio entre 1 ate o id_atual e consulta o alien
	correspondente dentro da lista de objetos, refaz a consulta ate encontrar
	um alien com o id aleatorio.
	*/
		consulta_alien_id(rand() % *id_atual, &col, &lin, l_aliens);

		while(col == -1){
			consulta_alien_id(rand() % *id_atual, &col, &lin, l_aliens);
		}
		insere_fim_lista(TIRO_ALIEN, *id_atual_tiros, l_tiros, lin+2, col+2, 0, 0,0);
		*id_atual_tiros = *id_atual_tiros + 1;

	}
}	