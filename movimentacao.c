/*  
arquivo para criar as animacoes de movimentacao
dentro do game
*/

#include <stdio.h>
#include <unistd.h>
#include <ncurses.h>
#include "movimentacao.h"
#include "skins.h"

void movimenta_aliens(t_lista *l){
	if(!lista_vazia(l)){
		int i, tam_lista;
		tamanho_lista(&tam_lista,l);
		l->atual = l->ini->prox;

		for(i = 1; i <= tam_lista; i++){

			/* verifica o sentindo atual do alien e o movimenta para esse*/
			if((l->atual->tipoelem <= ALIEN3_SKIN2) && (l->ini->sentido == RIGHT)){
				l->atual->elemcol++;

			}
			else if((l->atual->tipoelem <= ALIEN3_SKIN2) && (l->ini->sentido == LEFT)){
				l->atual->elemcol--;
			}

			l->atual = l->atual->prox;
		}

		/* detecta se os aliens chegaram na parede esquerda ou direita e troca a orientacao, descendo uma linha */
			if(detecta_colisao_parede(l)){

				if(l->ini->sentido == RIGHT){
					desce_uma_linha(l);
					l->ini->sentido = LEFT;
				}
				else{
					desce_uma_linha(l);
					l->ini->sentido = RIGHT;
				}

			}

	}
}

int detecta_colisao_parede(t_lista *l){
	if(lista_vazia(l))
		return 0;

	int i,tam_lista;

	tamanho_lista(&tam_lista, l);
	l->atual = l->ini->prox;

	/* caminha na lista ate achar um alien que esta no limite definido */
	for(i = 1; i <= tam_lista; i++){
		if((l->atual->tipoelem <= ALIEN3_SKIN2) && (l->atual->elemlin == LIMITE_BOTTOM))
			l->ini->tipoelem = MORREU;

		if((l->atual->tipoelem <= ALIEN3_SKIN2) && ((l->atual->elemcol == LIMITE_RIGHT) || (l->atual->elemcol == LIMITE_LEFT)))
			return 1;

		l->atual = l->atual->prox;
	}
	return 0;

}

void desce_uma_linha(t_lista *l){
	if(!lista_vazia(l)){
		int i, tam_lista;
		tamanho_lista(&tam_lista,l);
		/* move o objeto na direcao desejada */

		l->atual = l->ini->prox;

		for(i = 1; i <= tam_lista; i++){
			if(l->atual->tipoelem <= ALIEN3_SKIN2){
			l->atual->elemlin++;
			l->atual->speed = l->atual->speed - 394;

			if(l->ini->sentido == RIGHT)
				l->atual->elemcol--;
			else
				l->atual->elemcol++;
		}
			l->atual = l->atual->prox;
		}
	}
}


void troca_skin(t_lista *l){
	if(!lista_vazia(l)){
		int i, tam_lista;

		tamanho_lista(&tam_lista,l);

		l->atual = l->ini->prox;

		for(i = 1; i <= tam_lista; i++){

			if(l->atual->tipoelem == ALIEN1){
				l->atual->tipoelem = ALIEN1_SKIN2;
			}
			else if(l->atual->tipoelem == ALIEN1_SKIN2){
				l->atual->tipoelem = ALIEN1;
			}
			else if(l->atual->tipoelem == ALIEN2){
				l->atual->tipoelem = ALIEN2_SKIN2;
			}
			else if(l->atual->tipoelem == ALIEN2_SKIN2){
				l->atual->tipoelem = ALIEN2;
			}
			else if(l->atual->tipoelem == ALIEN3){
				l->atual->tipoelem = ALIEN3_SKIN2;
			}
			else if(l->atual->tipoelem == ALIEN3_SKIN2){
				l->atual->tipoelem = ALIEN3;
			}

			l->atual = l->atual->prox;

		}

	}
}



int detecta_colisao_tiro(t_lista *l_tiros, t_lista *l_objetos, int timestamp, int *id_atual, int *placar){

	if(lista_vazia(l_tiros))
		return 0;

	int i,j, tam_lista_objetos, tam_lista_tiros, pode_colidir;

	tamanho_lista(&tam_lista_objetos, l_objetos);

	tamanho_lista(&tam_lista_tiros, l_tiros);

	l_tiros->atual = l_tiros->ini->prox;
	l_objetos->atual = l_objetos->ini->prox;

	for(i = 1; i <= tam_lista_tiros; i++){

		for(j = 1; j <= tam_lista_objetos; j++){

			if(!((l_objetos->atual->tipoelem == NOTHING) || ((l_objetos->atual->tipoelem == NOTHING_PERMANENTE)))){			
				int bouding_box1[2], bouding_box2[2];
				/* aqui verificamos se o tiro pertence a area do alien */
				bouding_box1[0] = l_objetos->atual->elemlin;
				bouding_box1[1] = l_objetos->atual->elemcol;
				bouding_box2[0] = l_objetos->atual->elemlin + 2;
			
			/* formatacao para a nave mae */
				if(l_objetos->atual->tipoelem == NAVEMAE){
					bouding_box1[1] = l_objetos->atual->elemcol - 7;
					bouding_box2[1] = l_objetos->atual->elemcol + 1;		
				}
				else if(l_objetos->atual->tipoelem == BARREIRA)
					bouding_box2[1] = l_objetos->atual->elemcol + 6;
				else if((l_objetos->atual->tipoelem == ALIEN1) || (	l_objetos->atual->tipoelem == ALIEN1_SKIN2))
					bouding_box2[1] = l_objetos->atual->elemcol + 3;
				else
					bouding_box2[1] = l_objetos->atual->elemcol + 4;

				pode_colidir = 0;
				if((l_objetos->atual->tipoelem == BARREIRA) || (l_objetos->atual->tipoelem == JOGADOR)){
					pode_colidir = 1;
				}

				/* aqui verificamos se o tiro eh de um alien ou do jogador */
				if(!((l_tiros->atual->tipoelem == TIRO_ALIEN) && (pode_colidir == 0)) ){

					if((l_tiros->atual->elemlin >= bouding_box1[0]) && (l_tiros->atual->elemcol >= bouding_box1[1]) && (l_tiros->atual->elemlin <= bouding_box2[0]) && (l_tiros->atual->elemcol <= bouding_box2[1])  ){
			
					/* caso o tiro tenha atingido o jogador, claramente o jogo acabou */
						if(l_objetos->atual->tipoelem == JOGADOR)
						l_objetos->ini->tipoelem = MORREU;

					/* verificamos se ja existe um nothing no lugar que foi atingido */
					int k, nao_verifica;
					nao_verifica = 0;
					t_nodo *p;
					p = l_objetos->atual;
					l_objetos->atual = l_objetos->ini->prox;
					for(k = 1; k <= tam_lista_objetos; k++){
						if(((l_objetos->atual->tipoelem == NOTHING) || l_objetos->atual->tipoelem == NOTHING_PERMANENTE) && (l_objetos->atual->elemcol == l_tiros->atual->elemcol) && (l_objetos->atual->elemlin == l_tiros->atual->elemlin))
							nao_verifica = 1;

						l_objetos->atual = l_objetos->atual->prox;
					}
					l_objetos->atual = p;

					/* incrementamos o placar conforme cada alien ou navemae morto */
					if(l_objetos->atual->tipoelem <= ALIEN3_SKIN2){
						if(l_objetos->atual->coluna == COLUNA1){
							*placar = *placar + 10;
						}
						else if(l_objetos->atual->coluna == COLUNA2){
							*placar = *placar + 4;
						}
						else if(l_objetos->atual->coluna == COLUNA3){
							*placar = *placar + 3;
						}
						else if(l_objetos->atual->coluna == COLUNA4){
							*placar = *placar + 2;
						}
						else if(l_objetos->atual->coluna == COLUNA5){
							*placar = *placar + 1;
						}
					}
					else if(l_objetos->atual->tipoelem == NAVEMAE){
						*placar = *placar + 100;
					}

				/* primeiro verificamos se o objeto que foi atingido nao eh uma explosao
				caso for, ele somente remove o objeto e nao anima novamente com a explosao.
				Simplesmente para nao ficar spammando explosao em um mesmo lugar.
				*/
					if(!nao_verifica){
						if(l_objetos->atual->tipoelem == EXPLOSAO){
							remove_item_lista(l_tiros->atual->id, l_tiros);
							remove_item_lista(l_objetos->atual->id, l_objetos);	
						}
						else if(l_objetos->atual->tipoelem == BARREIRA){
							insere_depois_da_barreira_lista(NOTHING, *id_atual, l_objetos, l_tiros->atual->elemlin, l_tiros->atual->elemcol, timestamp, 0);	
							*id_atual = *id_atual + 1;
							insere_fim_lista(EXPLOSAO, *id_atual, l_objetos, l_tiros->atual->elemlin-1, l_tiros->atual->elemcol-2, timestamp, 0,0);
							*id_atual = *id_atual + 1;
							remove_item_lista(l_tiros->atual->id, l_tiros);
						}
						else{
							remove_item_lista(l_tiros->atual->id, l_tiros);	
							if(l_objetos->atual->tipoelem == NAVEMAE){
							insere_fim_lista(EXPLOSAO, l_objetos->atual->id, l_objetos, l_objetos->atual->elemlin, l_objetos->atual->elemcol-5, timestamp, 0,0);
						}
						else{
							insere_fim_lista(EXPLOSAO, l_objetos->atual->id, l_objetos, l_objetos->atual->elemlin, l_objetos->atual->elemcol, timestamp, 0,0);
						}
						remove_item_lista(l_objetos->atual->id, l_objetos);

					}

							return 1;
						}
					}
				}
			}
		 		l_objetos->atual = l_objetos->atual->prox;
		}

				l_objetos->atual = l_objetos->ini->prox;
				l_tiros->atual = l_tiros->atual->prox;
	}
	return 0;
}


int detecta_colisao_alien(t_lista *l, int *id_atual, int timestamp){
	
	if(lista_vazia(l))
		return 0;

	l->atual = l->ini->prox;

	int i,j,k,t, tam_lista;
	t_nodo *p;

	tamanho_lista(&tam_lista, l);
	l->atual = l->ini->prox;
		
	for(i = 1; i <= tam_lista; i++){
		
		/* 
		verifica se o elemento eh um alien e esta na linha da barreira 
		ou adiante, para nao fazer comparacao desnecessaria 
		*/
		if((l->atual->tipoelem <= ALIEN3_SKIN2) && (l->atual->elemlin >= 29)){
		p = l->atual;
		l->atual = l->ini->prox;
			for(j = 1; j <= 4; j++){
			int achou_barreira = 0;

				for(k = 0; k <= 2; k++){
					for(t = 0; t <= 6; t++){
						if((verifica_bouding_box(p, l->atual->elemlin+k, l->atual->elemcol+t)) && (!(verifica_colisao(l, l->atual->elemlin+k, l->atual->elemcol+t)))){
							achou_barreira = 1;
							insere_fim_lista(EXPLOSAO, *id_atual, l, l->atual->elemlin+k-1, l->atual->elemcol+t-2, timestamp, 0,0);
				            *id_atual = *id_atual + 1;
							insere_depois_da_barreira_lista(NOTHING, *id_atual, l, l->atual->elemlin+k, l->atual->elemcol+t, timestamp, 0);
							*id_atual = *id_atual + 1;
						}

					}
				}
				
				if(achou_barreira)
					break;

				l->atual = l->atual->prox;
			}
		l->atual = p;
		}
		l->atual = l->atual->prox;

	}
return 0;
}

int verifica_bouding_box(t_nodo *n, int lin, int col){
	if((lin >= n->elemlin) && (col >= n->elemcol) && (lin <= n->elemlin + 2) && (col  <= n->elemcol + 4))
		return 1;
	return 0;
}

int verifica_colisao(t_lista *l, int lin, int col){

	t_nodo *indice;
	int i, tam_lista;
	
	indice = l->ini->prox;
	tamanho_lista(&tam_lista, l);

	for(i = 1; i <= tam_lista; i++){
		if((indice->tipoelem == NOTHING) && (indice->elemlin == lin) && (indice->elemcol == col))
			return 1;
		indice = indice->prox;
	}
	return 0;
}

void movimenta_navemae(t_lista *l){
	if(!lista_vazia(l)){

		l->atual = l->ini->prox;
		l->fim->tipoelem = NAVEMAE;

		while(l->atual->tipoelem != NAVEMAE){
			incrementa_atual(l);
		}

		if(l->atual == l->fim){

		}else{
		l->atual->elemcol++;	
		}
	}
}