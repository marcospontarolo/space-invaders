/* implementa as funcoes de inicializacao */
#include <stdio.h>
#include <ncurses.h>
#include "inicializacao.h"
#include "skins.h"




void desenha_borda(){
    int i;
    chtype horizontal_delimiter, vertical_delimiter;
    /* desenha a borda ao redor da tela de jogo, de dimensao 38 linhas e 100 colunas */

    horizontal_delimiter='-';
    /* define o caractere delimitador vertical */
    vertical_delimiter='|';
    for(i=0;i <=100; i++){
        move(0,i);
        addch(horizontal_delimiter);
        move(37,i);
        addch(horizontal_delimiter);
    }
    for (i=1;    i < 37; i++){
        move(i,0);
        addch(vertical_delimiter);
        move(i,100);
        addch(vertical_delimiter);
    }

}

void inicializa_aliens(int *id_atual, t_lista *l, int velocidade_aliens){
	
	int i, linha, col;
	/* seta o sentindo dos aliens para resetar o anterior (caso exista)*/
	l->ini->sentido = RIGHT;
	/* seta a lin e col para a primeira fileira de aliens */
	linha = 7;
	col = 1;

	for(i=1; i<=11; i++){

		/* insere um alien tipo 1 ao fim da lista */
		insere_fim_lista(ALIEN1, *id_atual, l, linha, col, 0, velocidade_aliens, COLUNA1);

		/* incrementa o id para ser unico */
		*id_atual = *id_atual + 1;

		/* incrementa a linha para posicionar o prox alien */
		col = col + 7;
	}

	/* reseta a lin e col para posicionar a proxima linha de aliens */
	linha = 11;
	col = 1;

		for(i=1; i<=11; i++){

		/* insere um alien tipo 2 ao fim da lista */
		insere_fim_lista(ALIEN2, *id_atual, l, linha, col, 0, velocidade_aliens, COLUNA2);

		/* incrementa o id para ser unico */
		*id_atual = *id_atual + 1;

		/* incrementa a linha para posicionar o prox alien */
		col = col + 7;
	}

	/* reseta a lin e col para posicionar a proxima linha de aliens */
	linha = 15;
	col = 1;

		for(i=1; i<=11; i++){

		/* insere um alien tipo 2 ao fim da lista */
		insere_fim_lista(ALIEN2, *id_atual, l, linha, col, 0, velocidade_aliens, COLUNA3);

		/* incrementa o id para ser unico */
		*id_atual = *id_atual + 1;

		/* incrementa a linha para posicionar o prox alien */
		col = col + 7;
	}

	/* reseta a lin e col para posicionar a proxima linha de aliens */
	linha = 19;
	col = 1;

		for(i=1; i<=11; i++){

		/* insere um alien tipo 2 ao fim da lista */
		insere_fim_lista(ALIEN3, *id_atual, l, linha, col, 0, velocidade_aliens, COLUNA4);

		/* incrementa o id para ser unico */
		*id_atual = *id_atual + 1;

		/* incrementa a linha para posicionar o prox alien */
		col = col + 7;
	}


		/* reseta a lin e col para posicionar a proxima linha de aliens */
	linha = 23;
	col = 1;

		for(i=1; i<=11; i++){

		/* insere um alien tipo 2 ao fim da lista */
		insere_fim_lista(ALIEN3, *id_atual, l, linha, col, 0, velocidade_aliens, COLUNA5);

		/* incrementa o id para ser unico */
		*id_atual = *id_atual + 1;

		/* incrementa a linha para posicionar o prox alien */
		col = col + 7;
	}
}

void inicializa_jogador(int *id_atual, t_lista *l, int *id_jogador){

	insere_fim_lista(JOGADOR, *id_atual, l, 35, 48, 0, 0, 0);

	*id_jogador = *id_atual;
	*id_atual = *id_atual + 1;
}

void inicializa_navemae(int *id_atual, t_lista *l){
	int i, lin, col;

	lin = 31;
	col = 17;

	for(i=1; i <= 4; i++){

		insere_fim_lista(BARREIRA, *id_atual, l, lin, col, 0, 0, 0);
		*id_atual = *id_atual + 1;
		
		col = col + 20;
	}
}

void inicializa_binding(int *id_atual, t_lista *l){
	int i, lin, col, lin_temp, col_temp;

	lin = 31;
	col = 17;

	for(i=1; i <= 4; i++){

		insere_depois_da_barreira_lista(NOTHING_PERMANENTE, *id_atual, l, lin, col, 0, 0);	
		*id_atual = *id_atual + 1;

		insere_depois_da_barreira_lista(NOTHING_PERMANENTE, *id_atual, l, lin, col+6, 0, 0);	
		*id_atual = *id_atual + 1;

		lin_temp = lin + 2;
		col_temp = col + 2;
		insere_depois_da_barreira_lista(NOTHING_PERMANENTE, *id_atual, l, lin_temp, col_temp, 0, 0);	
		*id_atual = *id_atual + 1;
		col_temp++;
		insere_depois_da_barreira_lista(NOTHING_PERMANENTE, *id_atual, l, lin_temp, col_temp, 0, 0);	
		*id_atual = *id_atual + 1;
		col_temp++;
		insere_depois_da_barreira_lista(NOTHING_PERMANENTE, *id_atual, l, lin_temp, col_temp, 0, 0);	
		*id_atual = *id_atual + 1;
		
		col = col + 20;
	}
}


int game_over(t_lista *l, t_lista *a, t_lista *b, int *id_atual, int *velocidade_aliens){
	if(l->ini->tipoelem == MORREU){
		return 1;
	}
	else if(l->ini->tipoelem == VENCEU){
		l->ini->tipoelem = 0;
		*velocidade_aliens = *velocidade_aliens - 400;
		inicializa_aliens(id_atual, l, *velocidade_aliens);
		reseta_barreira(l);
		destroi_lista(a);
		destroi_lista(b);
		inicializa_lista(a);
		inicializa_lista(b);
	}
	return 0;
}


void spawna_navemae(t_lista *l, int id_navemae){
	int col, lin;

	consulta_posicao_id(id_navemae, &col, &lin, l);

	if((col == -1) && (lin == -1)){
    	insere_fim_lista(NAVEMAE, id_navemae, l, 2, -1, 0, 0,0);
	}
	else if(col >= 107){
		remove_item_lista(id_navemae, l);
	}
}