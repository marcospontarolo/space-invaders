/* 
	Desenvolvido por Marcos Pontarolo para a disciplina CI1001 - Programacao 1
*/


#include "velocidades.h"


/* define o nodo de objetos */
struct t_nodo {
    int id;
    int tipoelem;
    int elemlin;
    int elemcol;
    int speed;
    int estado;
    int timestamp;
    int sentido;
    int coluna;
    struct t_nodo *prox;
    struct t_nodo *prev;
};
typedef struct t_nodo t_nodo;

/* define o tipo lista */
struct t_lista {
    t_nodo *ini;
    t_nodo *atual;
    t_nodo *fim;
    int tamanho;
};
typedef struct t_lista t_lista;


/*
  Cria uma lista vazia. Ela eh duplamente encadeada e tem sentinelas no
  inicio e no final. Tambem tem um apontador para um elemento qualquer.
*/
int inicializa_lista(t_lista *l);

/*
  Retorna 1 se a lista está vazia e zero caso contrário.
*/
int lista_vazia(t_lista *l);


/* insere um objeto ao fim da lista */
int insere_fim_lista(int item, int id, t_lista *l, int lin, int col, int timestamp, int velocidade, int coluna);

/* insere um objeto depois das barreira na lista
   necessario para o objeto nao se sobrepor ao outro no ncurses
 */
int insere_depois_da_barreira_lista(int item, int id, t_lista *l, int lin, int col, int timestamp, int velocidade);

/* se encontrar um objeto que tem o id pesquisado, remove ele da lista */
int remove_item_lista(int id, t_lista *l);

/* incrementa a lin ou col do objeto com o id dado, dependendo da direcao dada */
int atualiza_item_lista(int id, int direcao, t_lista *l);

/* incrementa a lin ou col de cada item de uma lista l para uma direcao */
int atualiza_lista(int direcao, t_lista *l);

/* incrementa o ponteiro atual */
void incrementa_atual(t_lista *l);

/* imprime uma lista na tela, usando as definicoes de skin.h e as cores definidas na main */
void imprime_lista(t_lista *l);

/* retorna o tamanho de uma lista em tam */
int tamanho_lista(int *tam, t_lista *l);

/* consulta a posicao de um objeto na tela dado seu id */
void consulta_posicao_id(int id, int *col, int *lin, t_lista *l);

/* consulta um alien com o id dado e retorna a posicao dele em lin e col
se nao encontrado um alien com esse id, retorna -1 na lin e col */
void consulta_alien_id(int id, int *col, int *lin, t_lista *l);

/* consulta velocidade de um tipo de objeto e o retorna em *velocidade  */
void consulta_velocidade_tipo(int tipo, int *velocidade, t_lista *l);

/* dado um tipo ALIEN, consulta se ainda existe existe ele da lista l, senao, estipula que o jogador venceu */
void consulta_gameover_tipo(int tipo, t_lista *l);

/* consulta o item apontado pelo atual da lista */
int consulta_item_atual(int *id,int *lin, int *col, int *tipoelem, int *estado, t_lista *atual);

/* destroi uma lista e libera todos seus espacos com free */
void destroi_lista(t_lista *l);

/* remove o objeto do fim de uma lista l*/
int remove_fim_lista(t_lista *l);

/* deleta a explosao conforme seu timestamp para nao ficar na tela */
int deleta_explosao(t_lista *l, int timestamp_atual);

/* consulta o id do ultimo elemento de uma lista l e volta em *id */
void consulta_id_ultimo(t_lista *l, int *id);

/* cria os tiros dos aliens com uma limitacao de 3 tiros */
void spawna_tiros_dos_aliens(t_lista *l_tiros, t_lista *l_aliens, int *id_atual, int *id_atual_tiros);

/* imprime a nave mae na tela, se houver */
void imprime_nave_mae_lista(t_lista *l);

/* reseta a barreira para o proximo nivel, ou seja, faz a recuperacao da barreira removendo os NOTHING da lista l */
void reseta_barreira(t_lista *l);