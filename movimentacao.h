
#include "lib_lista.h"

/* define os limites para os aliens */
#define LIMITE_RIGHT 95
#define LIMITE_LEFT 1
#define LIMITE_BOTTOM 33		


/* movimenta os aliens para a orientacao atual */
void movimenta_aliens(t_lista *l);

/* faz os aliens descerem uma linha */
void desce_uma_linha(t_lista *l);

/* faz a animacao dos aliens, trocando sua skin */
void troca_skin(t_lista *l);

/* dada uma lista de tiros e uma lista de objetos, detecta uma colisao entre os dois */
int detecta_colisao_tiro(t_lista *l_tiros, t_lista *l_objetos, int timestamp, int *id_atual, int *placar);

/* detecta a colisao do alien com uma barreira */
int detecta_colisao_alien(t_lista *l, int *id_atual, int timestamp);

/* detecta a colisao dos aliens com uma parede e troca a orientacao */
int detecta_colisao_parede(t_lista *l);

/* dado duas coordenadas lin col, verifica se o lin e col possuem um NOTHING, caso sim retorna 1 */
int verifica_colisao(t_lista *l, int lin, int col);

/* dado um nodo n, verifica se o ponto lin col se situa dentro da area do nodo */
int verifica_bouding_box(t_nodo *n, int lin, int col);

/* movimenta a nave mae para o lado direito */
void movimenta_navemae(t_lista *l);