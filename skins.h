

/* coluna de aliens */
#define COLUNA1 1
#define COLUNA2 2
#define COLUNA3 3
#define COLUNA4 4
#define COLUNA5 5


/* id para reconhecer os objetos na tela */
#define ALIEN1 1
#define ALIEN1_SKIN2 2


#define ALIEN2 3
#define ALIEN2_SKIN2 4


#define ALIEN3 5
#define ALIEN3_SKIN2 6

#define TIRO 9
#define TIRO_ALIEN 12

#define EXPLOSAO 10

#define NOTHING 11
#define NOTHING_PERMANENTE 14

#define NAVEMAE 13

#define BARREIRA 8
#define JOGADOR 7
#define DIREITA 2
#define ESQUERDA 1
#define BAIXO 4
#define CIMA 3
#define LEFT 1
#define RIGHT 2


/* constante de game over */
#define MORREU 666
#define VENCEU 777


/* skins para todos os elementos da tela */
/* alien tipo 1 

  A
 AMA
 /X\

*/
#define ALIEN1_CABECA "  A "
#define ALIEN1_TRONCO " AMA"
#define ALIEN1_PE " /X\\"
#define ALIEN1_SKIN2_PE " { } "

/* alien tipo 2

.v_v.
}WMW{
 / \

*/
#define ALIEN2_CABECA ".v_v."
#define ALIEN2_TRONCO "}WMW{"
#define ALIEN2_PE " / \\"
#define ALIEN2_SKIN2_PE "'( )'"

/* alien tipo 3

 nmn
dbMdb
_/-\_

*/

#define ALIEN3_CABECA " nmn"
#define ALIEN3_TRONCO "dbMdb"
#define ALIEN3_PE "_/-\\_"
#define ALIEN3_SKIN2_PE " <^>  "
/* nave mae

 /MMMMM\ 
AMoMoMoMA
 \/'-'\/ 

*/
#define NAVEMAE_SKIN1 "\\ "
#define NAVEMAE_SKIN2 "MA"
#define NAVEMAE_SKIN3 "/ "

#define NAVEMAE_SKIN4 "MM"
#define NAVEMAE_SKIN5 "Mo"
#define NAVEMAE_SKIN6 "'\\"

#define NAVEMAE_SKIN7 "MM"
#define NAVEMAE_SKIN8 "Mo"
#define NAVEMAE_SKIN9 "'-"

#define NAVEMAE_SKIN10 "/M"
#define NAVEMAE_SKIN11 "Mo"
#define NAVEMAE_SKIN12 "\\/"

#define NAVEMAE_SKIN13 " "
#define NAVEMAE_SKIN14 "A"
#define NAVEMAE_SKIN15 " "

/* barreira
 
 AMMMA
AMMMMMA
MM   MM

*/

#define BARREIRA_CABECA " AMMMA "
#define BARREIRA_TRONCO "AMMMMMA"
#define BARREIRA_PE "MM   MM"

/* player 

 /^\
MMMMM

*/

#define JOGADOR_CABECA " /^\\"
#define JOGADOR_PE "MMMMM"


/* explosao
 
 \'/ 
-   -
 /,\ 

*/

#define EXPLOSAO_CABECA  " \\'/ "
#define EXPLOSAO_TRONCO "-   -"
#define EXPLOSAO_PE " /,\\ "


